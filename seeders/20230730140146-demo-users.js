'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('USERS', [
      {
        login: "test10@gmail.com",
        password: "$2a$10$Rdr.yRvwK/Pl49C90muC7.0uTfw9egYpqTl7tFDpxWXQWTH8pb8nS",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        login: "test11@gmail.com",
        password: "$2a$10$Rdr.yRvwK/Pl49C90muC7.0uTfw9egYpqTl7tFDpxWXQWTH8pb8nS",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        login: "test12@gmail.com",
        password: "$2a$10$Rdr.yRvwK/Pl49C90muC7.0uTfw9egYpqTl7tFDpxWXQWTH8pb8nS",
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ]);
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('users', null, {});
  }
};
