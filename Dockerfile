FROM node:18.16.0

WORKDIR /home/ubuntu/ipr

COPY .env ./.env
COPY package.json ./package.json

COPY package-lock.json ./package-lock.json
RUN npm ci

COPY . .

EXPOSE 5000
RUN npm i -g sequelize-cli

CMD [ "npm", "run", "migrate" ]
CMD [ "npm", "run", "seed" ]
CMD [ "npm", "run", "start:dev" ]