export function createArray(n: number , m: number): Array<number> {
    if (n > m) {
        return Array.from({ length: n - m }, (_, index) => m + index + 1);
    }
    return Array.from({ length: m - n }, (_, index) => n + index);
}