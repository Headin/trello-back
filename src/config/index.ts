export default () => ({
    db_host: process.env.HOST_DB,
    db_port: Number(process.env.PORT_DB),
    db_username: process.env.USER_NAME_DB,
    db_password: process.env.PASSWORD_DB,
    db_database: process.env.NAME_DB,
});

