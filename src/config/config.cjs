require('dotenv').config();

const config = {
    "username": process.env.USER_NAME_DB,
    "password": process.env.PASSWORD_DB,
    "database": process.env.NAME_DB,
    "host": process.env.HOST_DB,
    "logging": true,
    "dialect": "mysql"
};

module.exports = {
    "development": config,
    "test": config,
    "production": config
};
