import {Injectable} from '@nestjs/common';
import {InjectModel} from "@nestjs/sequelize";
import {UserModel} from "./users.model";
import {CreateUserDto} from "./dto/create-user-dto";

@Injectable()
export class UsersService {
    constructor(@InjectModel(UserModel) private userRepo: typeof UserModel) {}

    async createUser(dto: CreateUserDto) {
        return this.userRepo.create(dto)
    }

    async getUserByLogin(login: string) {
        return this.userRepo.findOne({
            where: {login},
            include: { all: true },
        })
    }

    async getAll() {
        return this.userRepo.findAll({ attributes: { exclude: ['password'] } })
    }
}
