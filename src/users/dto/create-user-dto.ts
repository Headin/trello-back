import * as Joi from "joi";
import {ApiProperty} from "@nestjs/swagger";

export const createUserSchema = Joi.object({
    login: Joi
        .string()
        .email()
        .required(),
    password: Joi
        .string()
        .min(8)
        .max(64)
        .required(),
})

export class CreateUserDto {
    @ApiProperty({ example: 'test@gmail.com'})
    readonly login: string;

    @ApiProperty()
    readonly password: string;
}