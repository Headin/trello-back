import {Column, DataType, Model, Table} from "sequelize-typescript";
import {ApiProperty} from "@nestjs/swagger";

interface UserAttr {
    login: string;
    password: string;
}

@Table({tableName: 'USERS'})
export class UserModel extends Model<UserModel, UserAttr> {

    @ApiProperty({ example: '1' })
    @Column({ type: DataType.INTEGER, unique: true, autoIncrement: true, primaryKey: true })
    id: number;

    @ApiProperty({ example: 'test@gmail.com' })
    @Column({ type: DataType.STRING, unique: true, })
    login: string;

    @ApiProperty()
    @Column({ type: DataType.STRING, })
    password: string;
}