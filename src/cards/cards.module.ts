import {forwardRef, Module} from '@nestjs/common';
import {SequelizeModule} from "@nestjs/sequelize";
import {JwtModule} from "@nestjs/jwt";

import { CardsController } from './cards.controller';
import { CardsService } from './cards.service';
import {CardModel} from "./cards.model";
import {BoardModel} from "../boards/boards.model";
import {ColumnsModel} from "../columns/columns.model";
import {BoardsModule} from "../boards/boards.module";
import {ColumnsModule} from "../columns/columns.module";


@Module({
  controllers: [CardsController],
  providers: [CardsService],
  exports: [CardsService],
  imports: [
    SequelizeModule.forFeature([CardModel, BoardModel, ColumnsModel]),
    JwtModule,
    forwardRef(() =>BoardsModule),
    forwardRef(() => ColumnsModule),
  ],
})
export class CardsModule {

}
