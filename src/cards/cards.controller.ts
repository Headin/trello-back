import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    HttpStatus,
    Param,
    Patch,
    Post,
    UseGuards,
    UsePipes
} from '@nestjs/common';
import {ApiBearerAuth, ApiNoContentResponse, ApiQuery, ApiResponse, ApiTags} from "@nestjs/swagger";

import {CreateCardDto, createCardSchema} from "./dto/create-card-dto";
import {ChangeOrderCardDto, changeOrderCardSchema} from "./dto/change-order-dto";

import {CardsService} from "./cards.service";
import { JoiValidationPipe } from 'src/validation.pipe';
import {CardModel} from "./cards.model";

import controllers from "../const/controllers";
import {JwtAuthGuard} from "../auth/jwt-auth.guard";
import {OpenApiPaginationResponse, Paginated} from "../common/paginations/pagination.decorator";
import {PaginationOptionsDto} from "../common/paginations/pagination.dto";
import {ChangeColumnCardDto, changeColumnCardSchema} from "./dto/change-column-dto";

@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
@ApiTags(controllers.cards)
@Controller(controllers.cards)
export class CardsController {
    constructor(private cardsService: CardsService) { }

    @ApiResponse({ status: HttpStatus.CREATED, type: CardModel })
    @HttpCode(HttpStatus.CREATED)
    @Post()
    @UsePipes(new JoiValidationPipe(createCardSchema))
    createCard(@Body() cardDto: CreateCardDto) {
        return this.cardsService.crete(cardDto)
    }

    @ApiNoContentResponse()
    @HttpCode(HttpStatus.NO_CONTENT)
    @Delete(':id')
    removeCard(@Param('id') id: string) {
        return this.cardsService.remove(Number(id))
    }

    @ApiResponse({ status: HttpStatus.OK, type: CardModel })
    @Get(':id')
    getOneCard(@Param('id') id: string) {
        return this.cardsService.getOne(Number(id))
    }

    @ApiResponse({ status: HttpStatus.OK, type: CardModel })
    @Patch(':id/order')
    @UsePipes(new JoiValidationPipe(changeOrderCardSchema))
    changeCardOrder(@Param('id') id: string, @Body() dto: ChangeOrderCardDto) {
        return this.cardsService.changeOrderCard(Number(id), dto)
    }

    @ApiResponse({ status: HttpStatus.OK, type: CardModel })
    @Patch(`:id/${controllers.columns}`)
    @UsePipes(new JoiValidationPipe(changeColumnCardSchema))
    changeCardColumn(@Param('id') id: string, @Body() dto: ChangeColumnCardDto) {
        return this.cardsService.changeColumnCard(Number(id), dto)
    }

    @OpenApiPaginationResponse(CardModel)
    @Get()
    @ApiQuery({ type: PaginationOptionsDto })
    getAllCards(@Paginated() options: PaginationOptionsDto) {
        return this.cardsService.getAllCard(options);
    }
}
