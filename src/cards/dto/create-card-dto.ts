import * as Joi from "joi";
import {ApiProperty} from "@nestjs/swagger";
import {MAX_CARD_TITLE, MIN_CARD_TITLE} from "../../const/rules";

export const createCardSchema = Joi.object({
    title: Joi
        .string()
        .min(MIN_CARD_TITLE)
        .max(MAX_CARD_TITLE)
        .required(),
    columnId: Joi
        .number()
        .required(),
})

export class CreateCardDto {
    @ApiProperty()
    readonly title: string;

    @ApiProperty()
    readonly columnId: number;
}