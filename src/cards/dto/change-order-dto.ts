import * as Joi from "joi";
import {ApiProperty} from "@nestjs/swagger";

export const changeOrderCardSchema = Joi.object({
    order: Joi.number().required()
});

export class ChangeOrderCardDto {
    @ApiProperty()
    readonly order: number;
}