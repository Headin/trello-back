import * as Joi from "joi";
import {ApiProperty} from "@nestjs/swagger";

export const changeColumnCardSchema = Joi.object({
    columnId: Joi.number().required()
});

export class ChangeColumnCardDto {
    @ApiProperty()
    readonly columnId: number;
}