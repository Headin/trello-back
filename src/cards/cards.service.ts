import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import sequelize from "sequelize";
import {InjectModel} from "@nestjs/sequelize";

import {CardModel} from "./cards.model";
import {CreateCardDto} from "./dto/create-card-dto";
import {ColumnsService} from "../columns/columns.service";
import {PaginationOptionsDto} from "../common/paginations/pagination.dto";
import {PaginationResult} from "../common/paginations/pagination.result";
import orderType from "../const/orderType";
import {ChangeOrderCardDto} from "./dto/change-order-dto";
import {createArray} from "../helpers/Utils";
import {ChangeColumnCardDto} from "./dto/change-column-dto";

@Injectable()
export class CardsService {
    constructor(
        @InjectModel(CardModel) private cardRepo: typeof CardModel,
        private columnService: ColumnsService
    ) {}

    async getHighestOrder(columnId: number) {
        const highestOrderColumn = await this.cardRepo.findOne({
            where: { columnId },
            order: [['order', orderType.desc]],
        })
        return highestOrderColumn?.order || 0;
    }

    async crete(dto: CreateCardDto) {
        const { columnId } = dto;
        const column = await this.columnService.getOne(columnId);

        const { boardId } = column;

        const currentMaxOrder = await this.getHighestOrder(columnId)

        return this.cardRepo.create({ ...dto, order: currentMaxOrder + 1, boardId })
    }

    async getOne(id: number) {
        if (!id) {
            throw new HttpException('Bad Id', HttpStatus.BAD_REQUEST)
        }

        const searchItem = await this.cardRepo.findOne({ where: { id }})

        if ( !searchItem ) {
            throw new HttpException('Item not found', HttpStatus.BAD_REQUEST)
        }

        return searchItem
    }

    async remove(id: number) {
        const { columnId } = await this.getOne(id);

        const currentMaxOrder = await this.getHighestOrder(columnId);
        await this.changeOrderCard(id, { order: currentMaxOrder });

        return this.cardRepo.destroy({ where: { id }})
    }

    async getAllCard(options: PaginationOptionsDto) {
        const { limit, offset } = options;
        const items = await this.cardRepo.findAll({ limit, offset });
        const totalItems = await this.cardRepo.count();
        return new PaginationResult({
            items,
            limit,
            offset,
            totalItems
        })
    }

    async getAllByColumnId(columnId: number, options: PaginationOptionsDto) {
        const { limit, offset } = options;
        const items = await this.cardRepo.findAll({ limit, offset, where: { columnId }, } );
        const totalItems = await this.cardRepo.count({ where: { columnId } });

        return new PaginationResult({
            items,
            limit,
            offset,
            totalItems
        })
    }

    async getAllByBoardId(boardId: number, options: PaginationOptionsDto) {
        const { limit, offset } = options;
        const items = await this.cardRepo.findAll({ limit, offset, where: { boardId }, } );
        const totalItems = await this.cardRepo.count({ where: { boardId } });

        return new PaginationResult({
            items,
            limit,
            offset,
            totalItems
        })
    }

    async changeOrderCard(cardId: number, dto: ChangeOrderCardDto) {
        const { order, columnId } = await this.getOne(cardId);
        const newOrder = dto.order, currentOrder = order;
        const updateValueOrder = newOrder > currentOrder ? -1 : 1;

        const currentMaxOrder = await this.getHighestOrder(columnId);

        if ( newOrder > currentMaxOrder || newOrder < 0 ) {
            throw new HttpException('Order not valid', HttpStatus.BAD_REQUEST);
        }

        await this.cardRepo.update(
            { order: sequelize.literal(`\`order\` + ${updateValueOrder}`)},
            { where: { order: createArray(newOrder, currentOrder) }, } );
        await this.cardRepo.update({ order: newOrder }, { where: {id: cardId}});

        return this.getOne(cardId)
    }

    async changeColumnCard(cardId: number, dto: ChangeColumnCardDto) {
        const { columnId } = await this.getOne(cardId);
        const { columnId: newColumnId } = dto;

        await this.columnService.getOne(newColumnId);

        const currentMaxOrder = await this.getHighestOrder(columnId);
        const newMaxOrder = await this.getHighestOrder(newColumnId);

        await this.changeOrderCard(cardId, { order: currentMaxOrder });
        await this.cardRepo.update(
            { columnId: newColumnId, order: newMaxOrder + 1 },
            { where: { id: cardId }}
        );

        return this.getOne(cardId)
    }
}
