import {BelongsTo, Column, DataType, ForeignKey, Model, Table} from "sequelize-typescript";
import {ApiProperty} from "@nestjs/swagger";
import {BoardModel} from "../boards/boards.model";
import {ColumnsModel} from "../columns/columns.model";

interface CardCreationAttr {
    title: string;
    order: number;
    boardId: number;
    columnId: number;
}

@Table({tableName: 'CARDS'})
export class CardModel extends Model<CardModel, CardCreationAttr> {

    @ApiProperty({ example: '1' })
    @Column({ type: DataType.INTEGER, unique: true, autoIncrement: true, primaryKey: true })
    id: number;

    @ApiProperty({ example: 'string' })
    @Column({ type: DataType.STRING, allowNull: false })
    title: string;

    @ApiProperty({ example: '1' })
    @Column({ type: DataType.INTEGER, allowNull: false })
    order: number;

    @ForeignKey(() => BoardModel)
    @Column({ type: DataType.INTEGER, allowNull: false })
    boardId: number;

    @BelongsTo(() => BoardModel)
    board: BoardModel

    @ForeignKey(() => ColumnsModel)
    @Column({ type: DataType.INTEGER, allowNull: false })
    columnId: number;

    @BelongsTo(() => ColumnsModel)
    column: ColumnsModel
}