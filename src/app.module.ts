import { Module } from '@nestjs/common';
import {ConfigModule} from "@nestjs/config";
import { SequelizeModule } from "@nestjs/sequelize";
import { JwtModule } from "@nestjs/jwt";

import { CardsModule } from './cards/cards.module';
import { AuthService } from './auth/auth.service';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import {BoardsModule} from "./boards/boards.module";
import {ColumnsModule} from "./columns/columns.module";

import { UserModel } from "./users/users.model";
import { CardModel } from "./cards/cards.model";
import {BoardModel} from "./boards/boards.model";
import {ColumnsModel} from "./columns/columns.model";


@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env'
    }),
    SequelizeModule.forRoot({
      dialect: 'mysql',
      host: process.env.HOST_DB,
      port: Number(process.env.PORT_DB),
      username: process.env.USER_NAME_DB,
      password: process.env.PASSWORD_DB,
      database: process.env.NAME_DB,
      autoLoadModels: true,
      models: [BoardModel, ColumnsModel, CardModel, UserModel],
    }),
    CardsModule,
    AuthModule,
    UsersModule,
    BoardsModule,
    ColumnsModule,
    JwtModule.register({
      signOptions: {
        expiresIn: '24h'
      }}),
  ],
  providers: [AuthService],
})
export class AppModule {}
