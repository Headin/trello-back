import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    HttpStatus,
    Param,
    Patch,
    Post,
    UseGuards,
    UsePipes
} from '@nestjs/common';
import {ApiBearerAuth, ApiNoContentResponse, ApiQuery, ApiResponse, ApiTags} from "@nestjs/swagger";

import {JwtAuthGuard} from "../auth/jwt-auth.guard";
import controllers from "../const/controllers";
import {ColumnsService} from "./columns.service";
import {JoiValidationPipe} from "../validation.pipe";
import {ColumnsModel} from "./columns.model";
import {CreateColumnDto, createColumnSchema} from "./dto/create-column-dto";
import {OpenApiPaginationResponse, Paginated} from "../common/paginations/pagination.decorator";
import {PaginationOptionsDto} from "../common/paginations/pagination.dto";
import {CardsService} from "../cards/cards.service";
import {CardModel} from "../cards/cards.model";
import {ChangeOrderColumnDto, changeOrderColumnSchema} from "./dto/change-order-dto";

@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
@ApiTags(controllers.columns)
@Controller(controllers.columns)
export class ColumnsController {
    constructor(
        private columnsService: ColumnsService,
        private cardsService: CardsService,
    ) { }

    @ApiResponse({ status: HttpStatus.CREATED, type: ColumnsModel })
    @Post()
    @UsePipes(new JoiValidationPipe(createColumnSchema))
    create(@Body() columnDto: CreateColumnDto) {
        return this.columnsService.create(columnDto)
    }

    @ApiNoContentResponse()
    @HttpCode(HttpStatus.NO_CONTENT)
    @Delete(':id')
    removeColumn(@Param('id') id: string) {
        return this.columnsService.remove(Number(id))
    }

    @ApiResponse({ status: HttpStatus.OK, type: ColumnsModel })
    @Get(':id')
    getOneColumn(@Param('id') id: string) {
        return this.columnsService.getOne(Number(id))
    }

    @OpenApiPaginationResponse(ColumnsModel)
    @Get()
    @ApiQuery({ type: PaginationOptionsDto })
    getAllColumns(@Paginated() options: PaginationOptionsDto) {
        return this.columnsService.getAll(options);
    }

    @OpenApiPaginationResponse(CardModel)
    @Get(`:id/${controllers.cards}`)
    @ApiQuery({ type: PaginationOptionsDto })
    getColumnsBoard(@Param('id') id: string, @Paginated() options: PaginationOptionsDto) {
        return this.cardsService.getAllByColumnId(Number(id), options)
    }

    @ApiResponse({ status: HttpStatus.OK, type: ColumnsModel })
    @Patch(':id/order')
    @UsePipes(new JoiValidationPipe(changeOrderColumnSchema))
    changeColumnOrder(@Param('id') id: string, @Body() dto: ChangeOrderColumnDto) {
        return this.columnsService.changeOrderColumn(Number(id), dto)
    }
}
