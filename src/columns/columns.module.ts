import {forwardRef, Module} from '@nestjs/common';
import {SequelizeModule} from "@nestjs/sequelize";
import {JwtModule} from "@nestjs/jwt";

import { ColumnsController } from './columns.controller';
import {ColumnsService} from "./columns.service";
import {ColumnsModel} from "./columns.model";
import {BoardModel} from "../boards/boards.model";
import {CardModel} from "../cards/cards.model";
import {BoardsModule} from "../boards/boards.module";
import {CardsModule} from "../cards/cards.module";

@Module({
  controllers: [ColumnsController],
  providers: [ColumnsService],
  exports: [ColumnsService],
  imports: [
    SequelizeModule.forFeature([CardModel, BoardModel, ColumnsModel]),
    JwtModule,
    forwardRef(() => BoardsModule),
    forwardRef(() => CardsModule),
  ],
})
export class ColumnsModule {}
