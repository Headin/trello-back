import {BelongsTo, Column, DataType, ForeignKey, HasMany, Index, Model, Table} from "sequelize-typescript";
import {ApiProperty} from "@nestjs/swagger";
import {BoardModel} from "../boards/boards.model";
import {CardModel} from "../cards/cards.model";

interface ColumnsCreationAttr {
    title: string;
    boardId: number;
    order: number;
}

@Table({tableName: 'COLUMN'})
export class ColumnsModel extends Model<ColumnsModel, ColumnsCreationAttr> {

    @ApiProperty({ example: '1' })
    @Column({ type: DataType.INTEGER, unique: true, autoIncrement: true, primaryKey: true })
    id: number;

    @ApiProperty({ example: 'string' })
    @Column({ type: DataType.STRING, allowNull: false })
    title: string;

    @ForeignKey(() => BoardModel)
    @Column({ type: DataType.INTEGER, allowNull: false })
    @Index
    boardId: number;

    @ApiProperty({ example: '1' })
    @Column({ type: DataType.INTEGER, allowNull: false })
    order: number;

    @BelongsTo(() => BoardModel, 'boardId')
    board: BoardModel;

    @HasMany(() => CardModel)
    cards: CardModel[];
}