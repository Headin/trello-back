import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import {InjectModel} from "@nestjs/sequelize";
import sequelize from "sequelize";

import {ColumnsModel} from "./columns.model";
import {CreateColumnDto} from "./dto/create-column-dto";
import {BoardsService} from "../boards/boards.service";
import {PaginationOptionsDto} from "../common/paginations/pagination.dto";
import {PaginationResult} from "../common/paginations/pagination.result";
import orderType from "../const/orderType";
import {ChangeOrderColumnDto} from "./dto/change-order-dto";
import {createArray} from "../helpers/Utils";

@Injectable()
export class ColumnsService {
    constructor(
        @InjectModel(ColumnsModel) private columnsRepo: typeof ColumnsModel,
        private boardsService: BoardsService,
    ) {}

    async getHighestOrder(boardId: number) {
        const highestOrderColumn = await this.columnsRepo.findOne({
            where: { boardId },
            order: [['order', orderType.desc]],
        })
        return highestOrderColumn?.order || 0;
    }

    async create(dto: CreateColumnDto) {
        const { boardId } = dto;
        await this.boardsService.getOne(boardId);

        const currentMaxOrder = await this.getHighestOrder(boardId);

        return this.columnsRepo.create({...dto, order: currentMaxOrder + 1 });
    }

    async remove(id: number) {
        const { boardId } = await this.getOne(id);
        const currentMaxOrder = await this.getHighestOrder(boardId);
        await this.changeOrderColumn(id, { order: currentMaxOrder });

        return this.columnsRepo.destroy({ where: { id }});
    }
a
    async getAll(options: PaginationOptionsDto) {
        const { limit, offset } = options;
        const items = await this.columnsRepo.findAll({ limit, offset });
        const totalItems = await this.columnsRepo.count();
        return new PaginationResult({
            items,
            limit,
            offset,
            totalItems
        })
    }

    async getOne(id: number) {
        if (!id) {
            throw new HttpException('Bad Id', HttpStatus.BAD_REQUEST)
        }

        const searchItem = await this.columnsRepo.findOne({ where: { id }})

        if ( !searchItem ) {
            throw new HttpException('Item not found', HttpStatus.BAD_REQUEST)
        }

        return searchItem
    }

    async getAllByBoardId(boardId: number, options: PaginationOptionsDto) {
        const { limit, offset } = options;
        const items = await this.columnsRepo.findAll({ limit, offset, where: { boardId }, } );
        const totalItems = await this.columnsRepo.count({ where: { boardId } });

        return new PaginationResult({
            items,
            limit,
            offset,
            totalItems
        })
    }

    async changeOrderColumn(columnId: number, dto: ChangeOrderColumnDto) {
        const { boardId, order } = await this.getOne(columnId);
        const newOrder = dto.order, currentOrder = order;
        const updateValueOrder = newOrder > currentOrder ? -1 : 1;

        const currentMaxOrder = await this.getHighestOrder(boardId);

        if ( newOrder > currentMaxOrder || newOrder < 0 ) {
            throw new HttpException('Order not valid', HttpStatus.BAD_REQUEST);
        }

        await this.columnsRepo.update(
            { order: sequelize.literal(`\`order\` + ${updateValueOrder}`)},
            { where: { order: createArray(newOrder, currentOrder) }, } );
        await this.columnsRepo.update({ order: newOrder }, { where: {id: columnId}});

        return this.getOne(columnId)
    }
}
