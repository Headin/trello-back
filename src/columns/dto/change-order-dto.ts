import * as Joi from "joi";
import {ApiProperty} from "@nestjs/swagger";

export const changeOrderColumnSchema = Joi.object({
    order: Joi.number().required()
});

export class ChangeOrderColumnDto {
    @ApiProperty()
    readonly order: number;
}