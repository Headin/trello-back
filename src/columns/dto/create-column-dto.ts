import * as Joi from "joi";
import {MAX_COLUMN_TITLE, MIN_COLUMN_TITLE} from "../../const/rules";
import {ApiProperty} from "@nestjs/swagger";

export const createColumnSchema = Joi.object({
    title: Joi
        .string()
        .min(MIN_COLUMN_TITLE)
        .max(MAX_COLUMN_TITLE)
        .required(),
    boardId: Joi
        .number()
        .required(),
})

export class CreateColumnDto {
    @ApiProperty()
    readonly title: string;

    @ApiProperty()
    readonly boardId: number;
}