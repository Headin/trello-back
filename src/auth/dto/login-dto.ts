import * as Joi from "joi";
import {ApiProperty} from "@nestjs/swagger";

export const loginSchema = Joi.object({
    login: Joi
        .string()
        .email()
        .required(),
    password: Joi
        .string()
        .min(8)
        .max(64)
        .required(),
})

export class LoginDto {
    @ApiProperty({ example: 'test10@gmail.com' })
    readonly login: string;

    @ApiProperty({ example: 'Qwerty12' })
    readonly password: string;
}