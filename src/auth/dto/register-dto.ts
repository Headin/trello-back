import {ApiProperty} from "@nestjs/swagger";
import * as Joi from "joi";

export const registerSchema = Joi.object({
    login: Joi
        .string()
        .email()
        .required(),
    password: Joi
        .string()
        .min(8)
        .max(64)
        .required(),
})

export class RegisterDto {
    @ApiProperty({ example: 'test@gmail.com'})
    readonly login: string;

    @ApiProperty()
    readonly password: string;
}