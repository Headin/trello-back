import {Body, Controller, HttpStatus, Post, UsePipes} from '@nestjs/common';
import {ApiResponse, ApiTags} from "@nestjs/swagger";

import controllers from "../const/controllers";
import {AuthService} from "./auth.service";
import {LoginDto, loginSchema} from "./dto/login-dto";
import {JoiValidationPipe} from "../validation.pipe";
import {RegisterDto, registerSchema} from "./dto/register-dto";
import {AuthModel} from "./auth.model";

@ApiTags(controllers.auth)
@Controller(controllers.auth)
export class AuthController {
    constructor(private authService: AuthService) {};

    @ApiResponse({ status: HttpStatus.OK, type: AuthModel })
    @Post('login')
    @UsePipes(new JoiValidationPipe(loginSchema))
    login(@Body() loginDto: LoginDto) {
        return this.authService.login(loginDto)
    }

    @ApiResponse({ status: HttpStatus.CREATED, type: AuthModel })
    @Post('register')
    @UsePipes(new JoiValidationPipe(registerSchema))
    register(@Body() registerDto: RegisterDto) {
        return this.authService.register(registerDto)
    }
}
