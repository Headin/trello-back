import {HttpException, HttpStatus, Injectable, UnauthorizedException} from '@nestjs/common';
import {JwtService} from "@nestjs/jwt";

import * as crypt from "bcryptjs";

import {LoginDto} from "./dto/login-dto";
import {RegisterDto} from "./dto/register-dto";
import {UsersService} from "../users/users.service";
import {UserModel} from "../users/users.model";

@Injectable()
export class AuthService {

    constructor(private jwtService: JwtService, private usersService: UsersService) { }

    private async generateToken(user: UserModel) {
        const payload = { login: user.login, id: user.id }
        return {
            token: this.jwtService.sign(payload,
                {
                    secret: process.env.PRIVATE_KEY,
                })
        }
    }

    private async validateUser(userDto: LoginDto) {
        const user = await this.usersService.getUserByLogin(userDto.login);
        if (!user){
            throw new UnauthorizedException({ message: 'Invalid login or password'})
        }
        const passEquals = await crypt.compare(userDto.password, user.password);
        if (user && passEquals) {
            return user
        }
        throw new UnauthorizedException({ message: 'Invalid login or password'})
    }

    async login(loginDto: LoginDto) {
        const user = await this.validateUser(loginDto);
        return this.generateToken(user);
    }

    async register(registerDto: RegisterDto) {
        const user = await this.usersService.getUserByLogin(registerDto.login)

        if (user) {
            throw new HttpException('User with this login exists', HttpStatus.BAD_REQUEST)
        }

        const hashPassword = await crypt.hash(registerDto.password, 10);
        const newUser = await this.usersService.createUser({ ...registerDto, password: hashPassword });
        return this.generateToken(newUser);
    }

    async getAll() {
        return this.usersService.getAll()
    }
}
