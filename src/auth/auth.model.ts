import {Model} from "sequelize-typescript";
import {ApiProperty} from "@nestjs/swagger";

interface AuthModelAttr {
    token: string;
}

export class AuthModel extends Model<AuthModel> {
    @ApiProperty({ example: 'string' })
    token: string;
}