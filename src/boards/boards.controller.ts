import {Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, Post, UseGuards, UsePipes} from '@nestjs/common';
import {ApiBearerAuth, ApiNoContentResponse, ApiQuery, ApiResponse, ApiTags} from "@nestjs/swagger";

import {JwtAuthGuard} from "../auth/jwt-auth.guard";
import controllers from "../const/controllers";
import {BoardsService} from "./boards.service";
import {JoiValidationPipe} from "../validation.pipe";
import {CreateBoardDto, createBoardSchema} from "./dto/create-board";
import {BoardModel} from "./boards.model";
import {OpenApiPaginationResponse, Paginated} from "../common/paginations/pagination.decorator";
import {PaginationOptionsDto} from "../common/paginations/pagination.dto";
import {ColumnsModel} from "../columns/columns.model";
import {ColumnsService} from "../columns/columns.service";
import {CardsService} from "../cards/cards.service";
import {CardModel} from "../cards/cards.model";

@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
@ApiTags(controllers.boards)
@Controller(controllers.boards)
export class BoardsController {
    constructor(
        private boardsService: BoardsService,
        private columnService: ColumnsService,
        private cardsService: CardsService,
        ) {  }

    @ApiResponse({ status: HttpStatus.CREATED, type: BoardModel })
    @Post()
    @UsePipes(new JoiValidationPipe(createBoardSchema))
    createBoard(@Body() boardDto: CreateBoardDto) {
        return this.boardsService.create(boardDto)
    }

    @ApiNoContentResponse()
    @HttpCode(HttpStatus.NO_CONTENT)
    @Delete(':id')
    removeBoard(@Param('id') id: string) {
        return this.boardsService.remove(Number(id))
    }

    @ApiResponse({ status: HttpStatus.OK, type: BoardModel })
    @Get(':id')
    getOneBoard(@Param('id') id: string) {
        return this.boardsService.getOne(Number(id))
    }

    @OpenApiPaginationResponse(ColumnsModel)
    @Get(`:id/${controllers.columns}`)
    @ApiQuery({ type: PaginationOptionsDto })
    getColumnsBoard(@Param('id') id: string, @Paginated() options: PaginationOptionsDto) {
        return this.columnService.getAllByBoardId(Number(id), options)
    }

    @OpenApiPaginationResponse(CardModel)
    @Get(`:id/${controllers.cards}`)
    @ApiQuery({ type: PaginationOptionsDto })
    getCardsBoard(@Param('id') id: string, @Paginated() options: PaginationOptionsDto) {
        return this.cardsService.getAllByBoardId(Number(id), options)
    }

    @OpenApiPaginationResponse(BoardModel)
    @Get()
    @ApiQuery({ type: PaginationOptionsDto })
    getAllBoards(@Paginated() options: PaginationOptionsDto) {
        return this.boardsService.getAll(options);
    }
}
