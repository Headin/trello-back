import {Column, DataType, HasMany, Model, Table} from "sequelize-typescript";
import {ApiProperty} from "@nestjs/swagger";

import {CardModel} from "../cards/cards.model";
import {ColumnsModel} from "../columns/columns.model";

interface BoardCreationAttr {
    title: string;
}

@Table({tableName: 'BOARDS'})
export class BoardModel extends Model<BoardModel, BoardCreationAttr> {

    @ApiProperty({ example: '1' })
    @Column({ type: DataType.INTEGER, unique: true, autoIncrement: true, primaryKey: true })
    id: number;

    @ApiProperty({ example: 'string' })
    @Column({ type: DataType.STRING, allowNull: false })
    title: string;

    @HasMany(() => ColumnsModel)
    columns: ColumnsModel[];

    @HasMany(() => CardModel)
    cards: CardModel[]
}