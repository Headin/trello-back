import * as Joi from "joi";
import {ApiProperty} from "@nestjs/swagger";
import {MAX_BOARD_TITLE, MIN_BOARD_TITLE} from "../../const/rules";

export const createBoardSchema = Joi.object({
    title: Joi
        .string()
        .min(MIN_BOARD_TITLE)
        .max(MAX_BOARD_TITLE)
        .required(),
})

export class CreateBoardDto {
    @ApiProperty()
    readonly title: string;
}