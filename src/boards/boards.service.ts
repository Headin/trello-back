import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import {InjectModel} from "@nestjs/sequelize";
import {BoardModel} from "./boards.model";
import {CreateBoardDto} from "./dto/create-board";
import {PaginationOptionsDto} from "../common/paginations/pagination.dto";
import {PaginationResult} from "../common/paginations/pagination.result";

@Injectable()
export class BoardsService {
    constructor(@InjectModel(BoardModel) private boardRepo: typeof BoardModel) {
    }

    create(dto: CreateBoardDto) {
        return this.boardRepo.create(dto)
    }

    async remove(id: number) {
        await this.getOne(id);
        return this.boardRepo.destroy({ where: { id }})
    }

    async getOne(id: number) {
        if (!id) {
            throw new HttpException('Bad Id', HttpStatus.BAD_REQUEST)
        }

        const searchItem = await this.boardRepo.findOne({ where: { id }})

        if ( !searchItem ) {
            throw new HttpException('Item not found', HttpStatus.NOT_FOUND)
        }

        return searchItem
    }

    async getAll(options: PaginationOptionsDto) {
        const { limit, offset } = options;
        const items = await this.boardRepo.findAll({ limit, offset } );
        const totalCount = await this.boardRepo.count();

        return new PaginationResult({
            items,
            limit,
            offset,
            totalItems: totalCount
        })
    }
}
