 import {forwardRef, Module} from '@nestjs/common';
import {SequelizeModule} from "@nestjs/sequelize";
import {JwtModule} from "@nestjs/jwt";

import { BoardsController } from './boards.controller';
import {BoardsService} from "./boards.service";
import {BoardModel} from "./boards.model";
import {CardModel} from "../cards/cards.model";
import {ColumnsModel} from "../columns/columns.model";
import {ColumnsModule} from "../columns/columns.module";
 import {CardsModule} from "../cards/cards.module";

@Module({
  controllers: [BoardsController],
  providers: [BoardsService],
  exports: [BoardsService],
  imports: [
    SequelizeModule.forFeature([CardModel, BoardModel, ColumnsModel]),
    JwtModule,
    forwardRef(() => ColumnsModule),
    forwardRef(() => CardsModule),
  ]
})
export class BoardsModule {}
