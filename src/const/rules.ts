//BOARD
export const MIN_BOARD_TITLE = 3;
export const MAX_BOARD_TITLE = 200;

//CARD
export const MIN_CARD_TITLE = 1;
export const MAX_CARD_TITLE = 2000;

//COLUMN
export const MIN_COLUMN_TITLE = 3;
export const MAX_COLUMN_TITLE = 20;